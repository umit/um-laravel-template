<?php

namespace UMT\Template;

use \Config;
use \DateInterval;
use \File;

use UMT\Less;
use \Response;
use \Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\FileViewFinder;
use Whoops\Example\Exception;

class TemplateServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	}

	public function boot()
	{
		$this->package('umt/template');
		$this->registerRoutes();
		if (class_exists('Less')) {
			\Less::make(Config::get('template::application_less_file'));
		}
	}

	public function registerRoutes()
	{
		\View::composer('layouts.master', function ($view) {
			try {
				$view->nest('head_calls', 'partials.header')
					->nest('navigation', 'partials.navigation');
			} catch (\InvalidArgumentException $e) {

			}
		});
		\View::name('layouts.master', 'layout');
	}
}
